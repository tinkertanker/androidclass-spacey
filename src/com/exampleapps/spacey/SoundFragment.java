package com.exampleapps.spacey;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.exampleapps.spaceyutils.AudioPlayer;

public class SoundFragment extends Fragment {

	private AudioPlayer mAudioplayer;
	private Button mAudioButton;
	private boolean mIsPlaying;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_sound, parent, false);
		
		mAudioButton = (Button)view.findViewById(R.id.one_small_step_button);
		mAudioButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (mIsPlaying) {
					mAudioplayer.stop();
					mAudioButton.setText(R.string.one_small_step);
					mIsPlaying = false;
				} else {
					if (mAudioplayer == null) {
						mAudioplayer = new AudioPlayer(R.raw.onesmallstep);
					}
					mAudioplayer.play(getActivity().getApplicationContext());
					mAudioButton.setText("Playing...");
					mIsPlaying = true;
				}
			}
		});
		
		return view;
	}
	
}
