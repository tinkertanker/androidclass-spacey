package com.exampleapps.spacey;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;


public class ReminderFragment extends Fragment implements OnClickListener {

	private Button mReminderButton1;
	private Button mReminderButton2;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_reminder, parent, false);
		
		mReminderButton1 = (Button)view.findViewById(R.id.reminder_button_1);
		mReminderButton1.setTag(1);
		mReminderButton1.setOnClickListener(this);

		mReminderButton2 = (Button)view.findViewById(R.id.reminder_button_2);
		mReminderButton2.setTag(2);
		mReminderButton2.setOnClickListener(this);
		
		return view;
	}

	@Override
	public void onClick(View view) {
		
		// set alarm for 5pm not implemented, sorry...
		
	    Intent alarmIntent = new Intent(getActivity(), ReminderReceiver.class);
	    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, 
	    		alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
	    AlarmManager alarmManager = (AlarmManager)getActivity()
	    		.getSystemService(Context.ALARM_SERVICE);
	    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 5000, 
	    		pendingIntent);
	}
	
}
