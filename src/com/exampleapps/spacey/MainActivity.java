package com.exampleapps.spacey;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends Activity implements ActionBar.TabListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ActionBar bar = getActionBar();
	    bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	    bar.addTab(bar.newTab().setText("Inspire Me").setTabListener(this).setTag(1));
	    bar.addTab(bar.newTab().setText("Read News").setTabListener(this).setTag(2));
	    bar.addTab(bar.newTab().setText("Set Reminder").setTabListener(this).setTag(3));
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		int tag = ((Integer) tab.getTag()).intValue();
		
		switch(tag) {
		case 1:
			Log.d("SpaceyLog", "Inspire Me");
			ft.replace(R.id.fragmentContainer, new SoundFragment());
			break;
		case 2:
			Log.d("SpaceyLog", "Read News");
			ft.replace(R.id.fragmentContainer, new NewsFragment());
			break;
		case 3:
			ft.replace(R.id.fragmentContainer, new ReminderFragment());
			Log.d("SpaceyLog", "Notify Me");
			break;			
		default:
			Log.e("SpaceyLog", "Shouldn't have reached here!");
		}
		
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		
	}

	

}
