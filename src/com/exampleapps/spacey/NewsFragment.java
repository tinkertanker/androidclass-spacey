package com.exampleapps.spacey;

import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import at.theengine.android.simple_rss2_android.RSSItem;
import at.theengine.android.simple_rss2_android.SimpleRss2Parser;
import at.theengine.android.simple_rss2_android.SimpleRss2ParserCallback;

public class NewsFragment extends Fragment {

	private TextView mTitleView;
	private TextView mPubDate;
	private TextView mDescription;
	private List<RSSItem> mRssList;
	private Button mPrevButton;
	private Button mNextButton;
	private int mIndex;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mIndex = 0;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_news, parent, false);
		
		mTitleView = (TextView)view.findViewById(R.id.title);
		mPubDate = (TextView)view.findViewById(R.id.pubDate);
		mDescription = (TextView)view.findViewById(R.id.description);
		
		SimpleRss2Parser parser = new SimpleRss2Parser("http://www.nasa.gov/rss/dyn/breaking_news.rss", 
		    new SimpleRss2ParserCallback() {
		        @Override
		        public void onFeedParsed(List<RSSItem> items) {
		            for(int i = 0; i < items.size(); i++){
		                Log.d("SpaceyLog",items.get(i).getTitle());
		                Log.d("SpaceyLog",items.get(i).getDescription());
		                Log.d("SpaceyLog",items.get(i).getDate());
		            }
		            mRssList = items;
		            updateRSS();
		        }
		        @Override
		        public void onError(Exception ex) {
		            Toast.makeText(getActivity().getApplicationContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
		        }
		});
		parser.parseAsync();
		
		mNextButton = (Button)view.findViewById(R.id.next_button);
		mNextButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (mIndex < mRssList.size() - 1) {
					mIndex++;
				}
				updateRSS();
			}
		});
		
		mPrevButton = (Button)view.findViewById(R.id.prev_button);
		mPrevButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (mIndex > 0) {
					mIndex--;
				}
				updateRSS();
			}
		});

		
		return view;
	}
	
	private void updateRSS() {
        mTitleView.setText(mRssList.get(mIndex).getTitle());
        mPubDate.setText(mRssList.get(mIndex).getDate());
        mDescription.setText(mRssList.get(mIndex).getDescription());
		
	}
	
}
