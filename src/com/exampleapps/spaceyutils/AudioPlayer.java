package com.exampleapps.spaceyutils;

import android.content.Context;
import android.media.MediaPlayer;

public class AudioPlayer {

    private MediaPlayer mPlayer;
    private int mAudioFileID;
    
    public AudioPlayer(int audioFileID) {
    	mAudioFileID = audioFileID;
    }
    
    public void stop() {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    public void play(Context c) {
        
        stop();

        mPlayer = MediaPlayer.create(c, mAudioFileID);
        
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                stop();
            }
        });

        mPlayer.start();
    }
    
    public boolean isPlaying() {
        return mPlayer != null;
    }
    
}
